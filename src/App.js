import React from "react";
import correctText from "./correctText";
import { useState } from "react";

import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyA7CsOFzmMu8H461dD2oG4ltUKJ3jttIp4",
  authDomain: "etymology-3db86.firebaseapp.com",
  projectId: "etymology-3db86",
  storageBucket: "etymology-3db86.appspot.com",
  messagingSenderId: "135287650846",
  appId: "1:135287650846:web:e02fb726d6b9d2592f8c0f",
};

initializeApp(firebaseConfig);

export default function App() {
  const [searchedWord, setSearchedWord] = useState("");
  const [data, setData] = useState([]);

  let apiCall = "";

  const handleSubmit = (event) => {
    event.preventDefault();
    apiCall =
      "https://api.wordnik.com/v4/word.json/" +
      searchedWord.toLowerCase() +
      "/etymologies?useCanonical=false&api_key=iezi89f6d9ubrmn8yaqs5lcx2txsmqlql8jpne2f2ca9yvmob";
    callApi(apiCall);
  };

  function callApi(apiCall) {
    fetch(apiCall)
      .then((response) => response.json())
      .then((data) => setData(data));
  }

  let messageElements = "";

  let message = "No results found";
  if (data.error) {
    messageElements = (
      <div>
        <button
          className="wordButton"
          onClick={() => {
            apiCall =
              "https://api.wordnik.com/v4/word.json/no/etymologies?useCanonical=false&api_key=iezi89f6d9ubrmn8yaqs5lcx2txsmqlql8jpne2f2ca9yvmob";
            callApi(apiCall);
            setSearchedWord("No");
          }}
        >
          No
        </button>
        <button
          className="wordButton"
          onClick={() => {
            apiCall =
              "https://api.wordnik.com/v4/word.json/results/etymologies?useCanonical=false&api_key=iezi89f6d9ubrmn8yaqs5lcx2txsmqlql8jpne2f2ca9yvmob";
            callApi(apiCall);
            setSearchedWord("results");
          }}
        >
          results
        </button>
        <button
          className="wordButton"
          onClick={() => {
            apiCall =
              "https://api.wordnik.com/v4/word.json/found/etymologies?useCanonical=false&api_key=iezi89f6d9ubrmn8yaqs5lcx2txsmqlql8jpne2f2ca9yvmob";
            callApi(apiCall);
            setSearchedWord("found");
          }}
        >
          found
        </button>
      </div>
    );
  } else if (data.message) {
    messageElements = "Server OVERLOAD";
  } else if (data[0]) {
    message = data[0].replace('<?xml version="1.0" encoding="UTF-8"?>', "");
    message = correctText(message);
  }

  if (data[0]) {
    let indivWordsArray = message.split(" ");

    messageElements = indivWordsArray.map((word) => {
      return (
        <button
          className="wordButton"
          onClick={() => {
            let apiWord = word.replace(/[_\W]+/g, "");

            apiCall =
              "https://api.wordnik.com/v4/word.json/" +
              apiWord.toLowerCase() +
              "/etymologies?useCanonical=false&api_key=iezi89f6d9ubrmn8yaqs5lcx2txsmqlql8jpne2f2ca9yvmob";
            callApi(apiCall);
            setSearchedWord(apiWord);
          }}
        >
          {word}
        </button>
      );
    });
  }

  return (
    <div>
      <h1 className="title">Search etymology</h1>
      <form onSubmit={handleSubmit}>
        <input
          className="inputBox"
          type="text"
          id="inputBox"
          required
          value={searchedWord}
          onChange={(event) => setSearchedWord(event.target.value)}
        />

        <input className="submit" type="submit" value="Search" />
      </form>
      <div className="wordsContainer">{messageElements}</div>
    </div>
  );
}
